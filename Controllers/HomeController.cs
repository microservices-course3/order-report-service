using Microsoft.AspNetCore.Mvc;

namespace OrderReportService.Controllers
{
    [Route("api/v1/")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        [HttpGet]
        public IActionResult Index()
        {
            return Ok(new { message = "Order Report Service v1" });
        }

        [HttpGet("healthz")]
        public IActionResult Health()
        {
            return Ok();
        }
    }
}
